syntax match bqName /\v^\<?[^\>]+\>/
syntax match bqName /\v^\([^\)]+\):?/
syntax match bqName /\v^[^\:\>\)]+\:/
syntax match bqStatement /\v\*+\s.*$/
syntax match bqStatement /\v^--\>\s.*$/
syntax match bqInfo /\v^Quote\sid:\s+\d+$/
syntax match bqInfo /\v^Quote\sscore:\s+\d+$/
syntax match bqInfo /\v^Quote:$/

highlight default link bqName Identifier
highlight default link bqStatement Constant
highlight default link bqInfo Statement

syntax sync fromstart

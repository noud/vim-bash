syntax match bqId /\v^\s+\d+/
syntax match bqScore /\v\|\s+\d+/hs=s+1
syntax match bqQuote /\v\|\s[^\|]+$/hs=s+1

highlight default link bqId Identifier
highlight default link bqScore Constant
highlight default link bqQuote Statement

syntax sync fromstart

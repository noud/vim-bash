let g:bq_db_path = expand('<sfile>:p:h:h') . "/quotes.db"
let g:bq_sqlite = "sqlite3"
let g:bq_number_of_quotes = 50

command! BashQuotes call bash#TopList()

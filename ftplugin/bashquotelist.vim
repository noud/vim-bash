set buftype=nofile

nnoremap <buffer> q :q<cr>
setlocal cursorline

setlocal statusline=%!bash#ListStatusLine()

nnoremap <buffer> t :call bash#TopList()<cr>
nnoremap <buffer> s :call bash#SearchList()<cr>
nnoremap <buffer> r :call bash#RandomList()<cr>

nnoremap <buffer> n :call bash#ListNext()<cr>
nnoremap <buffer> p :call bash#ListPrevious()<cr>

nnoremap <buffer> <cr> :call bash#CurrentQuote()<cr>

if exists("#airline")
    let w:airline_disabled = 1
endif

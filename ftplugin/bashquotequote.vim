set buftype=nofile

nnoremap <buffer> q :buffer list.bash.org<cr>
nnoremap <buffer> n :call bash#NextQuote()<cr>
nnoremap <buffer> p :call bash#PreviousQuote()<cr>

setlocal statusline=%!bash#QuoteStatusLine()

if exists("#airline")
    let w:airline_disabled = 1
endif

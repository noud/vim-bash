let s:bq_list_buffer = "list.bash.org"
let s:bq_quote_buffer = "quote.bash.org"
let s:bq_page = 0
let s:bq_list = "top"
let s:bq_current_quote_id = 0
let s:bq_quote_ids=[]

function! bash#ListNext()
    let s:bq_page += 1
    call bash#NextList()
endfunction

function! bash#ListPrevious()
    if s:bq_page == 0
        echom "Already first page"
    else
        let s:bq_page -= 1
    endif
    call bash#NextList()
endfunction

function! bash#NextList()
    if s:bq_list == "top"
        let l:raw_list = bash#RawTopList()
    elseif s:bq_list == "random"
        let l:raw_list = bash#RawRandomList()
    elseif s:bq_list == "search"
        let l:raw_list = bash#Search(g:bq_search)
    endif
    if len(l:raw_list) == 0
        echom "No next page available"
    else
	    call bash#CreateListBuffer(bash#FilList(l:raw_list))
    endif
endfunction

function! bash#TopList()
    let s:bq_page = 0
    let s:bq_list = "top"
	call bash#CreateListBuffer(bash#FilList(bash#RawTopList()))
endfunction

function! bash#RandomList()
    let s:bq_page = 0
    let s:bq_list = "random"
	call bash#CreateListBuffer(bash#FilList(bash#RawRandomList()))
endfunction

function! bash#SearchList()
    let s:bq_list = "search"
    call bash#StoreSearch()
	call bash#CreateListBuffer(bash#FilList(bash#Search(g:bq_search)))
endfunction

function! bash#RawTopList()
    return split(bash#Query("select * from quotes order by score desc limit ".g:bq_number_of_quotes." offset ".(g:bq_number_of_quotes*s:bq_page).";"), '\n\n')
endfunction

function! bash#RawRandomList()
    return split(bash#Query("select * from quotes where score > 0 order by random() limit ".g:bq_number_of_quotes." offset ".(g:bq_number_of_quotes*s:bq_page).";"), '\n\n')
endfunction

function! bash#StoreSearch()
	call inputsave()
  	let g:bq_search = input('Bash quote search: ')
  	call inputrestore()
endfunction

function! bash#Search(search)
    return split(bash#Query('select * from quotes where quote like "%' . a:search . '%" order by score desc limit '.g:bq_number_of_quotes.' offset '.(g:bq_number_of_quotes*s:bq_page).';'), '\n\n')
endfunction

function! bash#Query(query)
	let l:cmd = g:bq_sqlite . " -line " . g:bq_db_path . " '" . a:query . "'"
	let l:system_result = system(cmd)
    if v:shell_error != 0
        throw "BASHORG: system call to " . g:bq_sqlite . " failed. Query: " . l:cmd
    endif
    return l:system_result
endfunction

function! bash#CurrentQuote()
    let s:bq_current_quote_id = bash#CurrentLineQuoteId()
    call bash#CreateQuoteBuffer(bash#FetchQuote(s:bq_current_quote_id))
endfunction

function! bash#PreviousQuote()
    if len(s:bq_quote_ids) > 0 && s:bq_current_quote_id > 0
        let l:current_index = index(s:bq_quote_ids, s:bq_current_quote_id)
        if l:current_index > 0
            let s:bq_current_quote_id = s:bq_quote_ids[(l:current_index-1)]
            call bash#FillQuoteBuffer(bash#FetchQuote(s:bq_current_quote_id))
        else
            echom "This is the first quote."
        endif
    else
        echom "Can't find what quote we're at."
    endif
endfunction

function! bash#NextQuote()
    if len(s:bq_quote_ids) > 0 && s:bq_current_quote_id > 0
        let l:current_index = index(s:bq_quote_ids, s:bq_current_quote_id)
        if l:current_index > -1 && l:current_index < len(s:bq_quote_ids)
            let s:bq_current_quote_id = s:bq_quote_ids[(l:current_index+1)]
            call bash#FillQuoteBuffer(bash#FetchQuote(s:bq_current_quote_id))
        else
            echom "This is the last quote."
        endif
    else
        echom "Can't find what quote we're at."
    endif
endfunction

function! bash#CurrentLineQuoteId()
    let l:list_line = getline('.')
    return bash#Strip(split(l:list_line, '|')[0])
endfunction

function! bash#FetchQuote(quote_id)
	let l:quote_buffer = []
    let l:raw_quote = bash#Query('select * from quotes where id = "' . a:quote_id . '" limit 1;')
    let l:quote = bash#ProcessQuote(l:raw_quote)

	call add(l:quote_buffer, "Quote id:    " . l:quote.id)
	call add(l:quote_buffer, "Quote score: " . l:quote.score)
	call add(l:quote_buffer, "Quote:")
    call extend(l:quote_buffer, l:quote.body)
    
    return l:quote_buffer
endfunction

function! bash#FilList(raw_list)
    let s:bq_quote_ids=[]
	let l:quote_list = []
    let l:ww=bash#WindowWidth()
	for l:raw_quote in a:raw_list
        let l:quote = bash#ProcessQuote(l:raw_quote)
        call add(s:bq_quote_ids, l:quote.id)

		let l:new_quote = printf("%10s | %10s | %.".(l:ww - 27)."s", l:quote.id, l:quote.score, l:quote.body[0])
		call add(l:quote_list, l:new_quote)
	endfor
    return l:quote_list
endfunction

function! bash#ProcessQuote(raw_quote)
    let l:quote_parts = split(a:raw_quote, '\n')
    let l:quote = { 'id'    : bash#Strip(split(l:quote_parts[0], '=')[1]),
                  \ 'score' : bash#Strip(split(l:quote_parts[1], '=')[1]),
                  \ 'body'  : l:quote_parts[2:-1]
                  \ }
    let l:quote.body[0] = bash#Strip(split(l:quote.body[0],'=')[1])
    return l:quote
endfunction

function! bash#CreateListBuffer(quote_list)
    call bash#ReuseBuffer(s:bq_list_buffer)

	execute "0,$delete"

	call append(0, a:quote_list)
    
    execute "normal dd"
    execute 0
endfunction

function! bash#CreateQuoteBuffer(quote)
    call bash#ReuseBuffer(s:bq_list_buffer)
	if (bufexists(s:bq_quote_buffer))
		execute "buffer ".s:bq_quote_buffer
	else
		execute "edit ".s:bq_quote_buffer
	endif

    call bash#FillQuoteBuffer(a:quote)
endfunction

function! bash#FillQuoteBuffer(quote)
	execute "0,$delete"

	call append(0, a:quote)
    
    execute "normal dk"
    execute 0
endfunction

function! bash#ReuseBuffer(buffer_name)
	if (bufexists(a:buffer_name))
		let l:bash_list_win_nr = bufwinnr(a:buffer_name)
		if (l:bash_list_win_nr == -1)
			execute "sbuffer " . a:buffer_name
		else
			execute l:bash_list_win_nr . 'wincmd w'
		endif
	else
		execute "new " . a:buffer_name
	endif
endfunction

function! bash#Strip(input_string)
	return substitute(a:input_string,'\v^\s*(.{-})\s*$','\1','')
endfunction

function! bash#WindowWidth()
    let l:current_virt = &virtualedit
    set virtualedit=all

    execute "normal! g$"
    let l:bq_window_width = virtcol('.')
    let &virtualedit=l:current_virt

    return l:bq_window_width
endfunction

function! bash#ListStatusLine()
    return "bash.org quotes: ".s:bq_list." (".(s:bq_page*g:bq_number_of_quotes+1)."-".((s:bq_page+1)*g:bq_number_of_quotes).") search quote: s; top: t; random: r; next page: n; previous page: p"
endfunction

function! bash#QuoteStatusLine()
    return "bash.org quote; next quote: n; previous quote: p; return: q"
endfunction

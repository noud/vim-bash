augroup bashOrg
autocmd!

autocmd BufRead,BufNewFile list.bash.org   setlocal filetype=bashquotelist
autocmd BufRead,BufNewFile quote.bash.org  setlocal filetype=bashquotequote

augroup END
